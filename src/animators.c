#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "animators.h"

#define SEG_0	_BV(PB3)	/* . */
#define SEG_1	_BV(PB2)	/* - */
#define SEG_2	_BV(PB0)	/* / */
#define SEG_3	_BV(PB4)	/* | */
#define SEG_4	_BV(PB1)	/* \ */

static const uint8_t lut_numbers[] PROGMEM = {
	SEG_0 | SEG_1,			// -
	SEG_0,					// 1
	SEG_4,					// 2
	SEG_0 | SEG_4,			// 3
	SEG_2 | SEG_4,			// 4
	SEG_0 | SEG_2 | SEG_4,	// 5
	SEG_1 | SEG_2 | SEG_4,	// 6
	SEG_0 | SEG_1,			// -
};

static const uint8_t lut_ani[] PROGMEM = {
	/* ani 0 */
	SEG_1,
	SEG_2,
	SEG_3,
	SEG_4,
	SEG_1,
	SEG_2,
	SEG_3,
	SEG_4,

	/* ani 1 */
	SEG_4,
	SEG_4,
	SEG_2,
	SEG_2,
	SEG_4,
	SEG_4,
	SEG_2,
	SEG_2,

	/* ani 2 */
	SEG_1,
	SEG_1,
	SEG_3,
	SEG_3,
	SEG_1,
	SEG_1,
	SEG_3,
	SEG_3,

	/* ani 3 */
	SEG_4,
	SEG_3,
	SEG_2,
	SEG_1,
	SEG_4,
	SEG_3,
	SEG_2,
	SEG_1,

	/* ani 4 */
	SEG_0,
	SEG_1,
	SEG_0,
	SEG_2,
	SEG_0,
	SEG_3,
	SEG_0,
	SEG_4,

	/* ani 5 */
	SEG_0,
	SEG_4,
	SEG_0,
	SEG_3,
	SEG_0,
	SEG_2,
	SEG_0,
	SEG_1,
};

void show_number(uint8_t num) {
	PORTB = pgm_read_byte(&lut_numbers[num & 0x07]);
}

static void play(const uint8_t *lut) {
	uint8_t i;
	for (i = 0; i < 8; ++i) {
		PORTB = pgm_read_byte(lut + i);
		_delay_ms(400/8);
	}
}

void animate(uint8_t animation_id) {
	if (animation_id > 5)
		animation_id = 0;
	play(lut_ani + animation_id * 8);
}
