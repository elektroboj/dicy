#include <stdint.h>

#include "simplerand.h"


static uint16_t seed __attribute__ ((section (".noinit")));

uint16_t simplerand(void) {
	uint16_t i, j;
	i = seed >> 8;
	j = seed & 0xFF;
	i = i * 18273 + j;
	j = j * 29379 + i;
	seed = ((i & 0xFF) << 8) | (j & 0xFF);
	return seed;
}
