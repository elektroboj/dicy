#ifndef _ANIMATORS_H_
#define _ANIMATORS_H_

#include <stdint.h>

void animate(uint8_t animation_id);
void show_number(uint8_t num);

#endif
