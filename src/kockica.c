#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "simplerand.h"
#include "animators.h"


static void party_mode(void) {
	uint8_t aid;

	PORTB = 0x00;
	_delay_ms(80);

	for (;;) {
		aid = (uint8_t)simplerand() % 6;
		animate(aid);
	}
}

int main(void) {
	uint8_t random_number;
	uint8_t intro_anim_id;

	// init
	DDRB = _BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3) | _BV(PB4);
	ACSR = _BV(ACD);	// disable comparator

	// intro animation
	intro_anim_id = (simplerand() & 1) ? 0 : 3;
	animate(intro_anim_id);
	animate(intro_anim_id);

	// show the number
	random_number = (uint8_t)((uint8_t)simplerand() % 6) + 1;
	show_number(random_number);
	_delay_ms(1000);

	// party mode?
	if (random_number == 6) {
		party_mode();
	}

	// go to sleep
	PORTB = 0;
	for (;;) {
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		sleep_mode();
	}

	return 0;
}
