#ifndef _SIMPLE_RANDOM_H_
#define _SIMPLE_RANDOM_H_

#include <stdint.h>

uint16_t simplerand(void);

#endif
