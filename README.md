# Dicy

![Dicy image](doc/Dicy.jpg)

A simple electronic dice.

It goes into party mode if you get 6! ☺

See it in action: https://goo.gl/photos/2hGZx47mponNL12r7

# uDicy

In this repository there is a branch with a miniature version of the same device.

![uDicy image](doc/uDicy.jpg)
